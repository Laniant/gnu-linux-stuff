<b>Fix MTP issues instantly</b>

    simple-mtpfs
    
<b>Best comic / manga reader. Supports multiple libraries, .zip, .cbz, etc</b>

    ~~yacreader~~ Friendship ended with yacreader. Now .cbz previews via dolphin and nomacs are my best friends.
    
<b>Video editing. I use the pro version</b>

    davinci-resolve
    
<b>Image editing. Note: you have to run rawtherapee on it's own to get the full menues. 
Opening an image from the context menu will disable the file selector and queue</b>

    rawtherapee

<b>Image viewer</b>

    nomacs
       
<b>Web radio</b>

    shortwave
    
<b>The best media player</b>

    mpv
    
<b>The best music player (needs wine)</b>

    musicbee
