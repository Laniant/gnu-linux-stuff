# Generic Commands

<b>Crop images</b>

    mogrify -format png -crop 2100x1145+0+0 *.png

<b>Resize images</b>

    magick mogrify -resize 50% *.JPG

<b>Video size and other info</b>

    ffprobe -v quiet -print_format json -show_format -show_streams

<b>Change all .zip to .cbz</b>

    for i in *.zip; do mv "$i" "${i%.*}.cbz"; done

<b>Dependancies</b>

    ldd /opt/resolve/bin/resolve //change to install location

<b>Permissions change</b>

    chmod +x

<b>Start MariaDB</b>

    systemctl start mariadb

<b>Kill wine</b>

    wineserver -k9

<b>SMART best settings</b>

    sudo smartctl -a /dev/sda >> drive-report.txt

<b>Using JACK</b>

    pulseaudio --kill  
    jack_control  start
    <b>GPU Monitoring</b>
    nvidia-smi -l 1

# KDE Specific

<b>Remove KDE bar shadow</b>

    xprop -remove _KDE_NET_WM_SHADOW //then click the bar

# Arch/Manjaro/Arch-Based Specific

<b>Easiest way to get Japanese input (actual kana input)</b>

    https://www.ostechnix.com/setup-japanese-language-environment-arch-linux/

My advice is not to use fcitx or mozc, they alway seem to have issues with either typing kana or with working outside of gtk applications.

# Git Basics

<b>Clone repo</b>

    git clone git@gitlab.com:Laniant/laniant.git

<b>Update local instance</b>

    git pull

<b>Update the git</b>

    git add .
    git commit -m "second git from the terminal"
    git push origin master
https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html

https://rogerdudler.github.io/git-guide/
