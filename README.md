# GNU Linux Stuff

---Arch script / ~~Manjaro~~ KDE stuff / Bash commands---

[Useful Commands](https://gitlab.com/Laniant/gnu-linux-stuff/-/blob/master/Commands.md "Commands.md")

[Underrated Essential Programmes](https://gitlab.com/Laniant/gnu-linux-stuff/-/blob/master/Programs.md "Programs.md")

[How to make shortcuts to wine programs (KDE/Plasma)](https://gitlab.com/Laniant/gnu-linux-stuff/-/blob/master/MusicBee.desktop "MusicBee.desktop")

[Fixing stuff maintainers didn't](https://gitlab.com/Laniant/gnu-linux-stuff/-/blob/master/Troubleshooting.md "Troubleshooting.md")
