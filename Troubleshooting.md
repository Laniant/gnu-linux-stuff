# KDE Specific

<b>Can't scroll when mouse is at leftmost of screen?</b>

    Workspace Behavious > Touch Screen > Disable the left-side action 

<b>Breeze: remove task manager / panel / bar shadows.</b>

    Install and use the plasma theme Breeze-No-Shadow-Opaque-Panel

<b>Breeze: Make the titlebar the correct colour.</b>

    Window Decorations > edit > disable "Draw titlebar background gradient"

# Programme Specific

<b>LibreOffice: Hunspell not showing up</b>

    Install regional variant (e.g hunspell-en_AU)